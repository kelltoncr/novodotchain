# Dotchain


[![pipeline status](https://gitlab.com/yan.justino/dotchain/badges/master/pipeline.svg)](https://gitlab.com/yan.justino/dotchain/-/commits/master)

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Este obra está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Atribuição-NãoComercial 4.0 Internacional</a>.

Esse repositório de código contém artefatos de software utilizados durante as 
aulas da disciplina **TÓPICOS ESPECIAIS EM DESENVOLVIMENTO DE SOFTWARE 1 - 
Turma: 01 (2022.2)**, do INSTITUTO METROPOLE DIGITAL (IMD) / UFRN
. Nesse sentido, os códigos e aplicações da forma como estão aqui registrados devem ser
considerados como material utilizados para fins didáticos. Portanto, não representam uma sugestão ou insumo 
que deva ser aplicada para soluções reais.

