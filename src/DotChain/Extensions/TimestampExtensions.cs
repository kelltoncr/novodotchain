using System.Diagnostics.CodeAnalysis;
using DotChain.Models;
using NBitcoin;

namespace DotChain.Extensions;

[ExcludeFromCodeCoverage]
public static class TimestampExtensions
{
    public static long GetElapsedMinutes(this BlockMessage message)
    {
        const int secPerMinut = 60;
        var timestamp = message.Timestamp;
        var currentTime = DateTime.Now.ToUnixTimestamp();

        return (currentTime - timestamp) / secPerMinut;        
    }

    public static long GetCurrentTimestamp() => (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;    
}