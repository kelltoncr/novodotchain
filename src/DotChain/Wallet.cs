using DotChain.Extensions;
using NBitcoin;
using NBitcoin.Crypto;

namespace DotChain;

public class Wallet
{
    private string PrivateKey { get; }
    public string PublicAddress { get; }
    public string PublicKey { get; }

    public Wallet()
    {
        var privateKey = new Key(); // generate a random private key
        var testNetPrivateKey = privateKey.GetBitcoinSecret(Network.TestNet);  // generate our Bitcoin secret(also known as Wallet Import Format or simply WIF) from our private key for the testnet
        PrivateKey = testNetPrivateKey.ToString();
        PublicAddress = GetPublicAddress().ToString();
        PublicKey = GetRawPubKey();
    }

    private PubKey GetPubKey()
    {
        var secret = new BitcoinSecret(PrivateKey, Network.TestNet);
        return new PubKey(secret.PubKey.ToBytes().ToHexString());
    }
    
    private string GetRawPubKey()
    {
        return GetPubKey().ToBytes().ToBase64();
    }    
    
    private BitcoinPubKeyAddress GetPublicAddress()
    {
        var secret = new BitcoinSecret(PrivateKey, Network.TestNet);
        return secret.PubKeyHash.GetAddress(Network.TestNet);
    }
    
    public string Sign(string message)
    {
        var secret = new BitcoinSecret(PrivateKey, Network.TestNet);
        var sign = secret.PrivateKey.Sign(uint256.Parse(message.ToSha256()));
        return sign.ToDER().ToBase64();
    }

    public bool Verify(string message, string signature)
    {
        return Verify(message, signature, PublicKey);
    }   
    
    public static bool Verify(string message, string signature, string pubkey)
    {
        try
        {
            var pubKey = new PubKey(Convert.FromBase64String(pubkey).ToHexString());
            return pubKey.Verify(uint256.Parse(message.ToSha256()), new ECDSASignature(signature.FromBase64()));
        }
        catch (FormatException)
        {
            return false;
        }        
    }     
}