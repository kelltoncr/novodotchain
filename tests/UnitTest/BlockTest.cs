using FluentAssertions;
using UnitTest.Shared;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest;

public class BlockTest : Fixture
{
    public BlockTest(ITestOutputHelper outputHelper):base(outputHelper){ }
    
    [Fact]
    public void new_blocks_must_have_only_the_body()
    {
        Block.Hash.Should().BeNull();
        Block.Height.Should().Be(0);
        Block.Body.Should().NotBeNull();
        Block.Time.Should().Be(0);
        Block.PreviousBlockHash.Should().BeNull();
    }

    [Fact]
    public void new_block_should_not_be_valid()
    {
        Block.Validate().Should().BeFalse();
    }

    [Fact]
    public void blocks_can_decode_the_body()
    {
        var data = Block.GetData<Stub>();
        data.Should().BeEquivalentTo(Data);
    }
}